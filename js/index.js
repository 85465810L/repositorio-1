$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 5000
    });
    $('#contacto').on('show.bs.modal',function(e){
        console.log("Se esta mostrando la ventana modal");
        $("#btndigital").removeClass("btn-outline-success");
        console.log("Voy a remover la calse del boton");
        $("#btndigital").addClass("btn-primary");
        console.log("Se cambio a primary la calse del boton");
        $("#btndigital").prop("disabled",true);
    });
    $('#contacto').on('shown.bs.modal',function(e){
        console.log("Se mostro la ventana modal")
    });
    $('#contacto').on('hide.bs.modal',function(e){
        console.log("Se esta cerrando la ventana modal")
    });
    $('#contacto').on('hidden.bs.modal',function(e){
        console.log("Se cerro la ventana modal");
        $("#btndigital").prop("disabled",false);
    });
});